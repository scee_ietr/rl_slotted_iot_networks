%   UCB_Slotted_IoT: In this file we evaluate the performance of the UCB
%   algorithm in an IoT network. The total number of devices is stable but
%   the number of devices which uses the UCB algorithm changes
%

%   Other m-files required: Optimal_repartition.m

%   Subfunctions: none
%   MAT-files required: none
%
%   See also : none

%   This file is the code used for the simulations done in the paper:
%   Bonnefoi, R.; Besson, L.; Kaufmann, E.; Moy, C.; Palicot, J. 
%   �Multi-Armed Bandit Learning in IoT Networks:Learning helps even in 
%   non-stationary settings�, CROWNCOM, May 2017

%   For more details on the UCB algorithm, see:
%   Peter Auer, Nicol� Cesa-Bianchi, and Paul Fischer. 2002. Finite-time Analysis
%   of the Multiarmed Bandit Problem. Mach. Learn. 47, 2-3 (May 2002), 235-256.

%   Authors  : R�mi BONNEFOI(1) - Lilian BESSON (1)(2)
%   (1)SCEE Research Team - CentraleSup�lec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%   (2)SequeL Research Team - Inria Lille
%   40, avenue du Halley, 59650 Villeneuve d'Ascq, FRANCE
%   Email : first.last@centralesupelec.fr
%
%   Last revision : 20/10/2017

clear all;
close all;
clc;

% Frequency and time parameters
Nb_ch               = 10;
Nb_slots            = 1000000;
Nb_iter             = 50; % Must be higher than 2

% Devices
Nb_tot              = 2000; % Total number of devices
Trans_prob          = 0.001; % Probability that a end-device send a message
Repart_ch           = [0.3 0.2 0.1 0.1 0.05 0.05 0.02 0.08 0.01 0.09];
Prop_dyn            = 0.1:0.1:1;

% Metrics
Mean_opt            = zeros(1,length(Prop_dyn));
Mean_sub_opt        = zeros(1,length(Prop_dyn));
Mean_worst          = zeros(1,length(Prop_dyn));
Nb_int              = 50;
Suc_tot             = zeros(length(Prop_dyn),Nb_int);
D_ch_save           = zeros(length(Prop_dyn),Nb_ch);
Nb_stat_save        = zeros(length(Prop_dyn),Nb_ch);

for ind_dyn=1:1:length(Prop_dyn)
    
    Nb_dyn              = round(Prop_dyn(1,ind_dyn)*Nb_tot);
    
    Nb_stat                             = Nb_tot-Nb_dyn;
    Nb_stat_ch                          = round(Nb_stat*Repart_ch(1,1:Nb_ch-1));
    Nb_stat_ch                          = [Nb_stat_ch Nb_stat-sum(Nb_stat_ch)];
    Nb_stat_save(ind_dyn,:)             = Nb_stat_ch;
    % Metrics to assess performance
    Nb_slot_int                         = Nb_slots/Nb_int;
    Nb_trans_tot                        = zeros(Nb_iter,Nb_int);
    Nb_succ_tot                         = zeros(Nb_iter,Nb_int);

    % Probability to have a static device in a channel
    Prob_stat_in_ch                     = 1-(1-Trans_prob).^(Nb_stat_ch);


    % Learning strategy
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    for ind_iter = 1:1:Nb_iter

    % UCB parameters
    alpha           = 0.5;
    T               = zeros(Nb_dyn,Nb_ch);
    S               = zeros(Nb_dyn,Nb_ch);       % sum of rewards X =S/T;
    time_dev        = zeros(1,Nb_dyn);           % time use in the USB algorithm 

    % Metrics to assess performance
    Nb_trans        = zeros(1,Nb_int);
    Nb_succ         = zeros(1,Nb_int);
    ind             = 1;                        % Index of time interval

        for i =1:1:Nb_slots

            % Packet transmission by static devices
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            Static_in_ch            = rand(1,Nb_ch)<Prob_stat_in_ch;

            % Tranmission by dynamic devices
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % probability of transmission
            Devices                 = find(rand(1,Nb_dyn)<Trans_prob);

            ch_smart_dev            = zeros(1,length(Devices));
            for j=1:1:length(Devices)
                    % incrementation of time
                    time_dev(1,Devices(1,j))            = time_dev(1,Devices(1,j))+1;
                    Nb_trans(1,ind)                     = Nb_trans(1,ind)+1;
                    % UCB1 algorithm to choose the channel
                    T_zeros                             = T(Devices(1,j),:)==0;
                    T_temp                              = T(Devices(1,j),:);
                    T_temp(T_zeros)                     = 0.000000001;

                    A                                   = sqrt(alpha*log(time_dev(1,Devices(1,j)))./T_temp);

                    X                                   = S(Devices(1,j),:)./T_temp;
                    B                                   = X + A;
                    [value,ch]                          = max(B);
                    T(Devices(1,j),ch)                  = T(Devices(1,j),ch)+1;
                    ch_smart_dev(1,j)                   = ch;
            end

            % Successful transmission 
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            for j=1:1:Nb_ch
                cond                    = (Static_in_ch(1,j)==0) && sum((ch_smart_dev==j))==1;

                if cond,
                    S(Devices(1,ch_smart_dev==j),j)    = S(Devices(1,ch_smart_dev==j),j)+1;
                    Nb_succ(1,ind)                     = Nb_succ(1,ind)+1;
                end
            end

            % increment the interval index
            if mod(i/Nb_slot_int,1)==0,
                ind                                    = ind+1;
            end
        end

        % Results from UCB
        Nb_trans_tot(ind_iter,:)                            = Nb_trans;
        Nb_succ_tot(ind_iter,:)                             = Nb_succ;

    end
    
    Suc_tot(ind_dyn,:)                 = mean(Nb_succ_tot./Nb_trans_tot);
    
    % Ideal and worst case strategy
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Efficient sub_optimal strategy in which the users are inserted in the channels with the lowest number of devices
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Nb_ch_id                        = Nb_stat_ch;
    for k =1:1:Nb_dyn
        [M,I]                       = min(Nb_ch_id);
        Nb_ch_id(1,I)               = Nb_ch_id(1,I)+1; 
    end

    % Number of dynamic devices in each channels
    Nb_dyn_ch                       = Nb_ch_id-Nb_stat_ch;

    % probability to have a successful transmission by a dynamic device
    Mean_sub_opt(1,ind_dyn)         = (1/Nb_dyn)*sum((Nb_dyn_ch).*(1-Trans_prob).^(Nb_ch_id-1));
    
    % Optimal strategy
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [D_ch]                          = Optimal_repartition(Trans_prob,Nb_stat_ch,Nb_dyn);
    D_ch_save(ind_dyn,:)            = D_ch;              
    Mean_opt(1,ind_dyn)             = (1/sum(D_ch))*sum((D_ch).*(1-Trans_prob).^(D_ch+Nb_stat_ch-1));
    
    % Random strategy
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Mean_worst(1,ind_dyn)           = (mean((1-Trans_prob).^(Nb_stat_ch)))*(1-Trans_prob/Nb_ch)^(Nb_dyn-1);
end

Mean_sub_opt_plot   = transpose(Mean_sub_opt)*ones(1,Nb_int);
Mean_opt_plot       = transpose(Mean_opt)*ones(1,Nb_int);
Mean_worst_plot     = transpose(Mean_worst)*ones(1,Nb_int);

% figure;
% hold on;
% box on;
% grid on;
% plot(smooth(Suc_tot(10,:)));
% plot(Mean_opt_plot(10,:));
% plot(Mean_worst_plot(10,:));
% legend('UCB', 'Ideal','Random')
% xlim([1 50])
% ylim([0.7 1])

% global subplot
figure;
subplot;
subplot(5,2,1);
plot(1:50,smooth(Suc_tot(1,:)),1:50,Mean_worst_plot(1,:),1:50,Mean_opt_plot(1,:),1:50,Mean_sub_opt_plot(1,:));
legend('UCB','Random','Optimal','Good sub-optimal')
title('10%');
subplot(5,2,2);
plot(1:50,smooth(Suc_tot(2,:)),1:50,Mean_worst_plot(2,:),1:50,Mean_opt_plot(2,:),1:50,Mean_sub_opt_plot(2,:));
legend('UCB','Random','Optimal','Good sub-optimal')
title('20%');
subplot(5,2,3);
plot(1:50,smooth(Suc_tot(3,:)),1:50,Mean_worst_plot(3,:),1:50,Mean_opt_plot(3,:),1:50,Mean_sub_opt_plot(3,:));
legend('UCB','Random','Optimal','Good sub-optimal')
title('30%');
subplot(5,2,4);
plot(1:50,smooth(Suc_tot(4,:)),1:50,Mean_worst_plot(4,:),1:50,Mean_opt_plot(4,:),1:50,Mean_sub_opt_plot(4,:));
legend('UCB','Random','Optimal','Good sub-optimal')
title('40%');
subplot(5,2,5);
plot(1:50,smooth(Suc_tot(5,:)),1:50,Mean_worst_plot(5,:),1:50,Mean_opt_plot(5,:),1:50,Mean_sub_opt_plot(5,:));
legend('UCB','Random','Optimal','Good sub-optimal')
title('50%');
subplot(5,2,6);
plot(1:50,smooth(Suc_tot(6,:)),1:50,Mean_worst_plot(6,:),1:50,Mean_opt_plot(6,:),1:50,Mean_sub_opt_plot(6,:));
legend('UCB','Random','Optimal','Good sub-optimal')
title('60%');
subplot(5,2,7);
plot(1:50,smooth(Suc_tot(7,:)),1:50,Mean_worst_plot(7,:),1:50,Mean_opt_plot(7,:),1:50,Mean_sub_opt_plot(7,:));
legend('UCB','Random','Optimal','Good sub-optimal')
title('70%');
subplot(5,2,8);
plot(1:50,smooth(Suc_tot(8,:)),1:50,Mean_worst_plot(8,:),1:50,Mean_opt_plot(8,:),1:50,Mean_sub_opt_plot(8,:));
legend('UCB','Random','Optimal','Good sub-optimal')
title('80%');
subplot(5,2,9);
plot(1:50,smooth(Suc_tot(9,:)),1:50,Mean_worst_plot(9,:),1:50,Mean_opt_plot(9,:),1:50,Mean_sub_opt_plot(9,:));
legend('UCB','Random','Optimal','Good sub-optimal')
title('90%');
subplot(5,2,10);
plot(1:50,smooth(Suc_tot(10,:)),1:50,Mean_worst_plot(10,:),1:50,Mean_opt_plot(10,:),1:50,Mean_sub_opt_plot(10,:));
title('100%');
legend('UCB','Random','Optimal','Good sub-optimal')
